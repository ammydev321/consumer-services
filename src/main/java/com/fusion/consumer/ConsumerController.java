package com.fusion.consumer;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/consumers")
public class ConsumerController
{

	@GetMapping(path = "/", produces = "application/json")
	public ResponseEntity<String> getConsumer()
	{
		return new ResponseEntity<>("This is consumer ", HttpStatus.OK);
	}

}

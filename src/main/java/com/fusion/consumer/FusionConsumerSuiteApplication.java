package com.fusion.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FusionConsumerSuiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(FusionConsumerSuiteApplication.class, args);
	}

}
